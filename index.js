let instance = null;

class Publisher {
  #observers;
  constructor() {
    this.#observers = {};
    if (instance) {
      return instance;
    }
    instance = this;
  }

  on = (data, cb) => {
    if (this.#observers[data]) {
      this.#observers[data].push(cb);
    } else {
      this.#observers[data] = [cb];
    }
  };

  fire = data => {
    if (this.#observers[data]) {
      this.#observers[data.type].forEach(fn => {
        fn(data.payload);
      });
    }
  };

  off = (data, func) => {
    if (this.#observers[data]) {
      let index = this.#observers[data].indexOf(func);
      this.#observers[data].splice(index, 1);
    }
  };
}
